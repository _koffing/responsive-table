# Responsive Table Plugin #

The responsive table plugin allows a developer to easily add a fully responsive table to their project that not only resizes with the browser but also rearranges itself into a more easily digestible form. There is also an option for the table to not rearrange but be placed in a box that allows horizontal scrolling in mobile.

### To include into a project ###

* Add the javascript, css/scss, and either of the html samples into your project.

### Dependancies

* jQuery
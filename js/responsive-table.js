'use strict';

responsiveTable();

function responsiveTable() {
    $(".responsive-table").each(function(i){
        var $table = $(this);
        var labels = [];
        $table.children('tbody').children('tr').each(function (i) {
            if( i == 0 ){
                $(this).children('th').each(function(i){
                    if( i != 0 ){
                        labels.push($(this).text());
                    }
                });
            } else if (i != 0) {
                $(this).children('td').each(function(i){
                    if( i != 0 && labels.length >= i ){
                        $(this).append('<span class="mobile-label">'+ labels[i-1] +'</span>');
                    } else if ( i != 0 && labels.length < i ) {
                        $(this).append('<span class="mobile-label"></span>');
                    }
                });
            }
        });
    });

}
//# sourceMappingURL=app.js.map
